CLUSTER_NAME=devops-with-k8s-2020

cluster:
	k3d cluster create ${CLUSTER_NAME} --servers 3 --agents 3

clean:
	k3d cluster delete ${CLUSTER_NAME}